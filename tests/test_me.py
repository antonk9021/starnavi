# -*- coding: utf-8 -*-
"""
A set of tests for /users/me endpoint.
"""

import unittest
import json
from functools import reduce
from .test_base import BaseTest


class MyPageTest(BaseTest):
    """
    /users/me test class.
    """

    def test_authorized_can_view(self):
        """
        Test API capability to handle logged in user account view.
        """
        # Registering a default user with ID #1:
        headers = self.signup_and_login_headers()  # name: test, email: test@test.com
        result = self.client().get("/users/me", headers=headers)
        self.assertEqual(result.status_code, 200)
        self.assertSetEqual({"id", "name", "email", "posts", "last_login", "reg_date"},
                            set(result.json.keys()))
        self.assertDictEqual({"id": 1, "name": "test", "email": "test@test.com"},
                             self._subdict(result.json, "id", "name", "email"))

    def test_authorized_can_edit(self):
        """
        Test API capability to handle logged in user account details edit.
        """
        put_data = json.dumps({"name": "another", "email": "another@test.com", "password": "another"})
        headers = self.signup_and_login_headers()
        before_edit = self.client().get("/users/me", headers=headers)
        after_edit = self.client().put("/users/me", data=put_data, headers=headers)
        self.assertEqual(after_edit.status_code, 200)
        self.assertNotEqual(before_edit.json, after_edit.json)

    def test_authorized_can_delete(self):
        """
        Test API capability to handle logged in user account deletion.
        """
        put_data = json.dumps({"name": "another", "email": "another@test.com", "password": "another"})
        headers = self.signup_and_login_headers()

        delete = self.client().delete("/users/me", headers=headers)
        self.assertEqual(delete.status_code, 204)

        # Verifying that no get, put, or delete can be made after account deletion:
        after_deletion_get = self.client().get("/users/me", headers=headers)
        after_deletion_put = self.client().put("/users/me", data=put_data, headers=headers)
        after_deletion_delete = self.client().delete("/users/me", headers=headers)

        for result in [after_deletion_get, after_deletion_put, after_deletion_delete]:
            self.assertEqual(result.status_code, 404)
            self.assertDictEqual(result.json, {"error": "Your account was deleted"})

    def test_unauthorized_cannot_view(self):
        """
        Test API capability to block unauthorized account view.
        """
        get = self.client().get("/users/me")
        self.assertEqual(get.status_code, 401)
        self.assertDictEqual({"msg": "Missing Authorization Header"}, get.json)

    def test_unauthorized_cannot_edit(self):
        """
        Test API capability to block unauthorized account details edit.
        """
        data = json.dumps({"name": "test", "email": "test@test.com", "password": "test"})
        put = self.client().put("/users/me", data=data, mimetype="application/json")
        self.assertEqual(put.status_code, 401)
        self.assertDictEqual({"msg": "Missing Authorization Header"}, put.json)

    def test_unauthorized_cannot_delete(self):
        """
        Test API capability to prevent unauthorized account deletion.
        """
        delete = self.client().delete("/users/me")
        self.assertEqual(delete.status_code, 401)
        self.assertDictEqual({"msg": "Missing Authorization Header"}, delete.json)

    def test_privacy_edit(self):
        """
        Test whether user`s posts become private or public if the user
        respectively changes account privacy.
        """

        # Registering default private user with ID #1 and creating default post with ID #1.
        # User -- name: test, email: test@test.com; Post -- title: `test`, content: `test`
        post_owner_headers = self.create_post(private=True)

        # Trying to access created post without a JWT access token:
        unauthorized_should_fail = self.client().get("/posts/1")
        self.assertEqual(unauthorized_should_fail.status_code, 401)
        self.assertDictEqual({"error": "Login required"}, unauthorized_should_fail.json)

        # Changing the user`s privacy from public to private
        self.client().put("/users/me", data=json.dumps({"private": False}), headers=post_owner_headers)

        # Trying to access created post once more:
        unauthorized_should_succeed = self.client().get("/posts/1")
        self.assertEqual(unauthorized_should_succeed.status_code, 200)
        self.assertDictEqual({"title": "test", "content": "test"},
                             self._subdict(unauthorized_should_succeed.json, "title", "content"))

        # Creating another post with ID #2 by user #1:
        self.client().post("/posts/create", data=json.dumps({"title": "2nd post", "content": "second post"}),
                           headers=post_owner_headers)

        # At this moment both posts should be public:
        both_should_succeed = self.client().get("/posts")
        self.assertEqual(both_should_succeed.status_code, 200)
        self.assertDictEqual({"title": "test", "content": "test", "owner_id": 1},
                             self._subdict(both_should_succeed.json[0],
                                           "title", "content", "owner_id"))
        self.assertDictEqual({"title": "2nd post", "content": "second post", "owner_id": 1},
                             self._subdict(both_should_succeed.json[1], "title", "content", "owner_id"))

        # Making account private back should result in making all posts private:
        self.client().put("/users/me", data=json.dumps({"private": True}), headers=post_owner_headers)

        first_should_fail = self.client().get("/posts/1")
        second_should_fail = self.client().get("/posts/2")
        all_should_fail = self.client().get("/posts")

        for result in [first_should_fail, second_should_fail]:
            self.assertEqual(result.status_code, 401)
            self.assertDictEqual({"error": "Login required"}, result.json)
        self.assertEqual(all_should_fail.status_code, 404)
        self.assertDictEqual({"error": "No public posts have been found"}, all_should_fail.json)


if __name__ == '__main__':
    unittest.main()
