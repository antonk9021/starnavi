# -*- coding: utf-8 -*-
"""
A set of tests for /posts/<post_id>/like endpoint.
"""

import unittest
from .test_base import BaseTest


class LikeTest(BaseTest):
    """
    Post liking and unliking test class.
    """
    def test_others_can_like_post(self):
        """
        Test whether other users can like post written by its owner.
        """

        # Creating single default post with ID #1:
        self.create_post()
        post_liker_headers = self.signup_and_login_headers(name="another", email="another@test.com")

        # Liking the post with other user`s JWT token:
        like = self.client().post("/posts/1/like", headers=post_liker_headers)
        self.assertEqual(like.status_code, 200)
        self.assertDictEqual({"msg": f"You have liked the post #1"}, like.json)

    def test_others_can_unlike_post(self):
        """
        Test whether other users can unlike previously liked post.
        """

        # Creating single default post with ID #1:
        self.create_post()
        post_liker_headers = self.signup_and_login_headers(name="another", email="another@test.com")

        # Liking the post:
        self.client().post("/posts/1/like", headers=post_liker_headers)

        # Unliking the post with other user`s JWT token:
        unlike = self.client().delete("/posts/1/like", headers=post_liker_headers)
        self.assertEqual(unlike.status_code, 200)
        self.assertDictEqual({"msg": f"You have unliked the post #1"}, unlike.json)

    def test_cannot_assess_nonexistent(self):
        """
        Test API capability of blocking nonexistent posts (un)likes.
        """
        post_liker_headers = self.signup_and_login_headers(name="another", email="another@test.com")

        # (Un)liking the post that has not been previously created:
        like = self.client().post("/posts/1/like", headers=post_liker_headers)
        unlike = self.client().delete("/posts/1/like", headers=post_liker_headers)
        for result in [like, unlike]:
            self.assertEqual(result.status_code, 404)
            self.assertDictEqual({"error": f"Post #1 not found."}, result.json)

    def test_cannot_assess_own_post(self):
        """
        Test API capability to prevent user from (un)liking his or her own post.
        """

        # Creating single default post with ID #1:
        post_owner_headers = self.create_post()

        # Trying to (un)like created post with owner`s JWT token:
        like = self.client().post("/posts/1/like", headers=post_owner_headers)
        unlike = self.client().delete("/posts/1/like", headers=post_owner_headers)
        self.assertEqual(like.status_code, 403)
        self.assertEqual(unlike.status_code, 403)
        self.assertDictEqual({"error": "Sorry, you cannot like your own post"}, like.json)
        self.assertDictEqual({"error": "Sorry, you cannot unlike your own post"}, unlike.json)

    def test_cannot_like_twice(self):
        """Test API capability to prevent repeated (un)likes by the same user."""

        # Creating single default post with ID #1:
        self.create_post()
        post_liker_headers = self.signup_and_login_headers(name="another", email="another@test.com")

        # Trying to unlike a post that hasn`t been previously liked by the user:
        premature_unlike = self.client().delete("/posts/1/like", headers=post_liker_headers)
        self.assertEqual(premature_unlike.status_code, 403)
        self.assertDictEqual({"error": "You cannot unlike a post you haven\'t liked in the past"}, premature_unlike.json)

        # Liking the post:
        self.client().post("/posts/1/like", headers=post_liker_headers)

        # Trying to like it once more:
        repeated_like = self.client().post("/posts/1/like", headers=post_liker_headers)
        self.assertEqual(repeated_like.status_code, 403)
        self.assertDictEqual({"error": "Sorry, you cannot like the same post twice"}, repeated_like.json)

        # Unliking the post:
        self.client().delete("/posts/1/like", headers=post_liker_headers)

        # Trying to unlike it once more:
        repeated_unlike = self.client().delete("/posts/1/like", headers=post_liker_headers)
        self.assertEqual(repeated_unlike.status_code, 403)
        self.assertDictEqual({"error": "You cannot unlike a post you haven\'t liked in the past"}, repeated_unlike.json)

    def test_can_like_unliked(self):
        """
        Test whether it is possible to like previously unliked post.
        """

        # Creating single default post with ID #1:
        self.create_post()
        post_liker_headers = self.signup_and_login_headers(name="another", email="another@test.com")

        # Liking and unliking the post:
        self.client().post("/posts/1/like", headers=post_liker_headers)
        self.client().delete("/posts/1/like", headers=post_liker_headers)
        repeated_like = self.client().post("/posts/1/like", headers=post_liker_headers)

        self.assertEqual(repeated_like.status_code, 200)
        self.assertDictEqual({"msg": f"You have liked the post #1"}, repeated_like.json)


if __name__ == '__main__':
    unittest.main()
