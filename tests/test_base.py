# -*- coding: utf-8 -*-
"""
Base testing interface inherited by custom test classes.
"""

import unittest
import json
from app.app import create_app, db


class BaseTest(unittest.TestCase):
    """
    Base test class.
    """
    def setUp(self):
        self.app = create_app(config_name="testing")
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def signup_and_login_headers(self, name="test", email="test@test.com", password="test", private=False):
        """
        A helper method that registers and logs in a user with default parameters:
        `name`: `test`
        `email`: `test@test.com`
        `password`: `test`

        :return: Headers with valid JWT access token.
        """
        data = json.dumps({"name": name, "email": email, "password": password, "private": private})
        self.client().post("/users/signup", data=data, mimetype="application/json")
        result = self.client().post("/users/login", data=data, mimetype="application/json")
        access_token = result.json["access_token"]
        headers = {"Authorization": f"Bearer {access_token}", "Content-Type": "application/json"}

        return headers

    def create_post(self, title="test", content="test", private=False):
        """
        A helper method for post creation. Default parameters are:
        `title`: `test`
        `content`: `test`
        """
        post_owner_headers = self.signup_and_login_headers(private=private)
        post_content = {"title": title, "content": content}
        self.client().post("/posts/create", data=json.dumps(post_content), headers=post_owner_headers)

        # If necessary, post owner`s headers may be achieved after post creation:
        return post_owner_headers

    def _subdict(self, source_dict, *keys):
        """
        Select subdict of a dict by keys.
        :param source_dict: Dict to select from.
        :param keys: Keys to select.
        """
        return {key: value for key, value in source_dict.items() if key in keys}
