# -*- coding: utf-8 -*-
"""
A set of tests for /users/login endpoint.
"""

import unittest
import json
from .test_base import BaseTest


class LoginTest(BaseTest):
    """
    Users login test class.
    """
    def test_existing_user_can_login(self):
        """
        Test login capabilities.
        """

        # Registering new user:
        data = json.dumps({"name": "test", "email": "test@test.com", "password": "test"})
        self.client().post("/users/signup", data=data, mimetype="application/json")

        # Attempting login:
        result = self.client().post("/users/login", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 200)
        self.assertMultiLineEqual("Logged in as test", result.json["msg"])
        self.assertIsNotNone(result.json["access_token"])
        self.assertIsNotNone(result.json["refresh_token"])

    def test_nonexistent_user_cannot_login(self):
        """
        Test API blocking unregistered users login.
        """
        data = json.dumps({"email": "test@test.com", "password": "test"})

        # Trying to login with above user data without registering it:
        result = self.client().post("/users/login", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 404)
        self.assertDictEqual({"error": f"User with email test@test.com doesn\'t exist"}, result.json)

    def test_logged_in_cannot_signup(self):
        """
        Test API preventing logged in user from signing up.
        """

        # Registering and logging in the first user:
        first_user = json.dumps({"name": "test_1", "email": "test_1@test.com", "password": "test"})

        self.client().post("/users/signup", data=first_user, mimetype="application/json")
        first_user_result = self.client().post("/users/login", data=first_user, mimetype="application/json")

        # Trying to register the second user without logging out the first one:
        access_token = first_user_result.json["access_token"]
        headers = {"Authorization": f"Bearer {access_token}", "Content-Type": "application/json"}

        second_user = json.dumps({"name": "test_2", "email": "test_2@test.com", "password": "test"})
        second_user_result = self.client().post("/users/signup", data=second_user, headers=headers)
        self.assertDictEqual({"error": "Please log out prior to registering a new user"}, second_user_result.json)
        self.assertEqual(second_user_result.status_code, 403)


if __name__ == '__main__':
    unittest.main()
