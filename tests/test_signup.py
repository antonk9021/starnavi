# -*- coding: utf-8 -*-
"""
A set of tests for /users/signup endpoint.
"""

import unittest
import json
from .test_base import BaseTest


class SignupTest(BaseTest):
    """
    Registration errors test class.
    """
    def test_valid_registration(self):
        """
        Test API capability to handle valid signup data.
        """
        data = json.dumps({"name": "test",
                           "email": "test@test.com",
                           "password": "test"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 201)
        self.assertDictEqual({"msg": "User test has been created"}, result.json)

    def test_empty_data(self):
        """
        Test API capability to block empty requests on signup.
        """
        result = self.client().post("/users/signup", data=None, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": "Empty request"}, result.json)

    def test_invalid_data_format(self):
        """
        Test API capability to block data in invalid formats.
        """
        data = json.dumps({"name": 111, "email": 123, "password": 321, "private": "some_irrelevant_string"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": {
            "name": ["Not a valid string."],
            "email": ["Not a valid email address."],
            "password": ["Not a valid string."],
            "private": ["Not a valid boolean."]
        }}, result.json)

    def test_invalid_email(self):
        """
        Test API capability to block emails not fitting respective regex.
        """
        data = json.dumps({"name": "test", "email": "test", "password": "test"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": {"email": ["Not a valid email address."]}}, result.json)

    def test_lacking_data(self):
        """
        Test API capability to block data lacking necessary fields.
        """
        # Lacking name:
        data = json.dumps({"email": "test@test.com", "password": "test"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": {"name": ["Missing data for required field."]}}, result.json)

        # Lacking email:
        data = json.dumps({"name": "test", "password": "test"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": {"email": ["Missing data for required field."]}}, result.json)

        # Lacking password:
        data = json.dumps({"name": "test", "email": "test@test.com"})
        result = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertEqual(result.status_code, 400)
        self.assertDictEqual({"error": {"password": ["Missing data for required field."]}}, result.json)

    def test_repeated_user_signup(self):
        """
        Test API blocking repeated registration of the same user.
        """

        # Registering a new user:
        data = json.dumps({"name": "test",
                           "email": "test@test.com",
                           "password": "test"})
        registration = self.client().post("/users/signup", data=data, mimetype="application/json")

        # Trying to register a user with the same email:
        repeated = self.client().post("/users/signup", data=data, mimetype="application/json")
        self.assertNotEqual(registration.status_code, repeated.status_code)
        self.assertEqual(repeated.status_code, 400)
        self.assertDictEqual({"error": "User with email test@test.com already exists"}, repeated.json)

    def test_private_user_registration(self):
        """
        Test registration of user so nobody unauthorized can view his or her account.
        """
        data = json.dumps({"name": "test",
                           "email": "test@test.com",
                           "password": "test",
                           "private": True})
        self.client().post("/users/signup", data=data, mimetype="application/json")

        # Since the only user is being registered, his account ID will be #1.
        # Trying to access it without JWT access token:
        unauthorized_access = self.client().get("/users", mimetype="application/json")
        unauthorized_access_single = self.client().get("/users/1", mimetype="application/json")
        self.assertEqual(unauthorized_access.status_code, 404)
        self.assertEqual(unauthorized_access_single.status_code, 401)
        self.assertDictEqual({"error": "No public user accounts found"}, unauthorized_access.json)
        self.assertDictEqual({"error": "Login required"}, unauthorized_access_single.json)


if __name__ == '__main__':
    unittest.main()
