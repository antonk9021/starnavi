# -*- coding: utf-8 -*-
"""
A set of tests for /users/logout, /users/logout/access, /users/logout/refresh, and /token/refresh endpoints.
"""

import unittest
import json
from .test_base import BaseTest
from time import sleep


class LogoutTest(BaseTest):
    """
    Users logout test class.
    """
    def test_regular_logout(self):
        """
        Test whether logged in user can logout and respective token is revoked.
        """
        user_headers = self.signup_and_login_headers()  # name: test, email: test@test.com, password: test
        result = self.client().post("/users/logout", headers=user_headers)
        self.assertEqual(result.status_code, 200)
        self.assertDictEqual({"msg": "Access token has been revoked"}, result.json)

    def test_refresh_token_revoke(self):
        """
        Test JWT refresh token revoking. Normally, access token should fail
        and refresh token should pass on /users/logout/refresh.
        """

        # Registering a new user:
        user_data = json.dumps({"name": "test", "email": "test@test.com", "password": "test"})
        self.client().post("users/signup", data=user_data, mimetype="application/json")
        tokens = self.client().post("users/login", data=user_data, mimetype="application/json")

        # Setting up wrong and right headers (containing access and refresh tokens respectively)
        access_token = tokens.json["access_token"]
        refresh_token = tokens.json["refresh_token"]
        wrong_headers = {"Authorization": f"Bearer {access_token}", "Content-Type": "application/json"}
        right_headers = {"Authorization": f"Bearer {refresh_token}", "Content-Type": "application/json"}

        # Checking results:
        should_fail = self.client().post("/users/logout/refresh", headers=wrong_headers)
        should_succeed = self.client().post("/users/logout/refresh", headers=right_headers)
        self.assertEqual(should_fail.status_code, 422)
        self.assertDictEqual({"msg": "Only refresh tokens are allowed"}, should_fail.json)
        self.assertEqual(should_succeed.status_code, 200)
        self.assertDictEqual({"msg": "Refresh token has been revoked"}, should_succeed.json)

    def test_expired_token_refresh(self):
        """
        Test refresh of an expired token.
        """

        # Setting access token lifetime to one second:
        self.app.config["JWT_ACCESS_TOKEN_EXPIRES"] = 1

        # Using `/users/me` as an example endpoint that requires JWT token:
        protected_endpoint = "/users/me"

        # Registering and logging in a user:
        data = {"name": "test", "email": "test@test.com", "password": "test"}
        self.client().post("/users/signup", data=json.dumps(data), mimetype="application/json")
        user = self.client().post("/users/login", data=json.dumps(data), mimetype="application/json")
        outdated_headers = {"Authorization": f"Bearer {user.json['access_token']}", "Content-Type": "application/json"}
        refresh_headers = {"Authorization": f"Bearer {user.json['refresh_token']}", "Content-Type": "application/json"}

        # Waiting for token to definitely expire:
        sleep(2)

        # Trying to access protected point with an outdated token:
        should_fail = self.client().get(protected_endpoint, headers=outdated_headers)

        self.assertEqual(should_fail.status_code, 401)
        self.assertDictEqual({"msg": "Token has expired"}, should_fail.json)

        # Setting access token lifetime to one hour:
        self.app.config["JWT_ACCESS_TOKEN_EXPIRES"] = 3600

        # Refreshing the token:
        refreshed_token = self.client().post("/token/refresh", headers=refresh_headers)
        self.assertEqual(refreshed_token.status_code, 200)

        fresh_headers = {"Authorization": f"Bearer {refreshed_token.json['new access token']}", "Content-Type": "application/json"}

        # Trying to access protected point with a fresh token:
        should_succeed = self.client().get(protected_endpoint, headers=fresh_headers)
        self.assertEqual(should_succeed.status_code, 200)

        # Checking whether the same user is preserved despite new access token:
        self.assertDictEqual({"name": "test", "email": "test@test.com"},
                             self._subdict(should_succeed.json, "name", "email"))
