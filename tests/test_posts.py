# -*- coding: utf-8 -*-
"""
A set of tests for /posts and /posts/create endpoints.
"""

import unittest
import json
from .test_base import BaseTest


class PostsTest(BaseTest):
    """
    Post creation class.
    """
    def test_authorized_can_write_post(self):
        """
        Test whether logged in user can create a post.
        """
        headers = self.signup_and_login_headers()
        post_content = {"title": "test", "content": "test"}
        result = self.client().post("/posts/create", data=json.dumps(post_content), headers=headers)
        self.assertEqual(result.status_code, 201)
        self.assertDictEqual(post_content, self._subdict(result.json, "title", "content"))

    # def test_can_write_multiple_posts(self):  # TODO: This test seems useless, consider its deletion
    #     """
    #     Test whether logged in user can repeatedly create posts.
    #     """
    #     for i in range(1, 100+1):
    #         self.create_post(title=f"Post #{i}", content=f"This is post #{i}")
    #     result = self.client().get("/posts/100")
    #     self.assertEqual(result.status_code, 200)
    #     self.assertMultiLineEqual("Post #100", result.json["title"])
    #     self.assertMultiLineEqual("This is post #100", result.json["content"])

    def test_unauthorized_cannot_write_post(self):
        """
        Test whether API prevents unauthorized users from writing posts.
        """
        post_content = {"title": "test", "content": "test"}
        result = self.client().post("/posts/create", data=json.dumps(post_content))
        self.assertEqual(result.status_code, 401)
        self.assertDictEqual({"msg": "Missing Authorization Header"}, result.json)

    def test_unauthorized_cannot_view_private(self):
        """
        Test privacy behaviour: users under private/public accounts
        should always create private/public posts correspondingly.
        Unauthorized users should not view private posts.
        """

        # Creating single private post with ID #1:
        self.create_post(private=True)
        result = self.client().get("/posts")
        result_single = self.client().get("/posts/1")
        self.assertEqual(result_single.status_code, 401)
        self.assertEqual(result.status_code, 404)
        self.assertDictEqual({"error": "Login required"}, result_single.json)
        self.assertDictEqual({"error": "No public posts have been found"}, result.json)

    def test_authorized_can_edit_own(self):
        """
        Test whether logged in user can edit his or her own post.
        """

        # Creating single default post with ID #1:
        post_owner_headers = self.create_post()  # title: "test", content: "test
        changed_post = {"title": "another", "content": "another"}

        # Editing created post content:
        result = self.client().put("/posts/1", data=json.dumps(changed_post), headers=post_owner_headers)
        self.assertEqual(result.status_code, 200)
        self.assertMultiLineEqual(changed_post["title"], result.json["title"])
        self.assertMultiLineEqual(changed_post["content"], result.json["content"])

    def test_authorized_can_delete_own(self):
        """
        Test whether logged in user can delete his or her own post.
        """

        # Creating single default post with ID #1:
        post_owner_headers = self.create_post()  # title: "test", content: "test"

        # Deleting created post:
        result = self.client().delete("/posts/1", headers=post_owner_headers)
        self.assertEqual(result.status_code, 204)

    def test_owner_can_manipulate(self):
        """
        Test whether owner can edit/delete his or her post.
        """

        # Creating single default post with ID #1:
        owner_headers = self.create_post()  # title: "test", content: "test"
        changed_post = {"title": "another", "content": "another"}

        # Trying to edit and afterwards delete created post with owner`s JWT token:
        edit = self.client().put("/posts/1", data=json.dumps(changed_post), headers=owner_headers)
        delete = self.client().delete("/posts/1", headers=owner_headers)
        self.assertEqual(edit.status_code, 200)
        self.assertEqual(delete.status_code, 204)
        self.assertMultiLineEqual(changed_post["title"], edit.json["title"])
        self.assertMultiLineEqual(changed_post["content"], edit.json["content"])

    def test_others_cannot_manipulate(self):
        """
        Test whether API prevents other users from editing/deleting owner`s post.
        """

        # Creating single default post with ID #1:
        self.create_post()  # title: "test", content: "test"
        other_credentials = {"name": "other", "email": "other@test.com", "password": "123"}
        other_user_headers = self.signup_and_login_headers(**other_credentials)
        changed_post = {"title": "another", "content": "another"}

        # Trying to edit and afterwards delete created post with other user`s JWT token:
        edit = self.client().put("/posts/1", data=json.dumps(changed_post), headers=other_user_headers)
        delete = self.client().delete("/posts/1", headers=other_user_headers)
        self.assertEqual(edit.status_code, 403)
        self.assertEqual(delete.status_code, 403)
        self.assertDictEqual({"error": "Only post owner can edit it"}, edit.json)
        self.assertDictEqual({"error": "Only post owner can delete it"}, delete.json)

    def test_deleted_account_cannot_post(self):
        """
        Testing whether API prevents users from writing posts if their accounts were deleted
        even though they may have valid access tokens.
        """

        # Registering a user and deleting its account without revoking access token:
        user_headers = self.signup_and_login_headers()
        self.client().delete("/users/me", headers=user_headers)

        # Trying to write a post from deleted account using valid access token:
        post_content = json.dumps(({"title": "test", "content": "test"}))
        result = self.client().post("/posts/create", data=post_content, headers=user_headers)
        self.assertEqual(result.status_code, 403)
        self.assertDictEqual({"error": "You cannot create a post since your account was deleted"}, result.json)


if __name__ == '__main__':
    unittest.main()
