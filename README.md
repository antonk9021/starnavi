# A test assignment for starnavi.io

This is readme for Flask-based test assignment for starnavi.io.

## Requirements
- Python 3.6+
- Flask web framework
- Packages from `requirements.txt`

The project uses SQLite v.3 as its database for demonstration purposes.

## Installation and setup
- Create virtual environment if desired
- Install requirements with pip:
```
$ pip install -r requirements.txt
```
- Before first launch perform first database migration with:
```
$ python manage.py db init
$ python manage.py db migrate
$ python manage.py db upgrade
```

This will create *migrations* repository and *app.db* SQLite database file. **Warning**: please do not manually edit or delete these entities.

### Enviroment variables

Some capabilities of the app require corresponding environment variables to be set up (configuration through *.env* file is recommended):

- SECRET_KEY=*your_secret_key* - Flask secret key.
- JWT_SECRET_KEY=*your_secret_key* - needed for JWT authentication to work.
- EMAILHUNTER_KEY=*your_api_key_for_hunter.io* - needed for hunter.io email verification.
- CLEARBIT_KEY=*your_clearbit_api_key* - needed for Clearbit Entrichment to work.



## Run
- Launch with `python run.py`
- By default the application runs locally on 5000 port.

## Flask application structure
```
.
|──────app/
| |────__init__.py
| |────app.py
| |────exceptions.py
| |────models.py
| |────resources.py
|──────__init__.py
|──────config.py
|──────manage.py
|──────README.md
|──────requirements.txt
|──────run.py
|──────tests/

```

## Features

The project includes the following features:

- Accounts registration, editing, and deletion.
- JWT-based authentication, login, logout.
- Posts writing, editing, deletion, liking and unliking.
- Privacy settings: if a user decides to make his or her account private, unauthorized visitors will not be able to view neither its details nor this user's posts.
- Clearbit Enrichment for getting additional user data on signup.
- Emailhunter.co (hunter.io) email verification on signup. 

### Clearbit Enrichment

In this project, the following data is retrieved by Clearbit Enrichment from user's email on his or her signup:
```
- Personal info:
  - Given name
  - Family name
  - City
  - State
  - Country (by country code)
  - Professional biography abstract
  - Personal website
  
- Social networks:
  - Facebook, Github, Twitter, and Linkedin handles
  - Github ID
  - Number of Github and Twitter followers
  
- Current occupation:
  - Company
  - Title
  - Role
  - Subrole
  - Seniority
 
- Company data:
  - Name
  - Legal name
  - Sector
  - Industry 
  - SIC Code
  - NAICS Code
  - Headquaters location (city, state, and country code)
  - Phone
  - Domain name
  
- Company metrics: 
  - Total amount raised
  - US Alexa rank
  - Global Alexa rank
  - Annual revenue.
```

## API reference

The following endpoints support respective methods:

- **GET** /, **GET** /users - retrieve the list of all users. Private accounts may be viewed by logged in users only.
- **POST** /users/signup - new user registration.
- **POST** /users/login - existing user login.
- **POST** /users/logout, **POST** /users/logout/access - user logout with JWT access token revoking.
- **POST** /users/logout/refresh - user logout with JWT refresh token revoking.
- **GET** /users/user_id - look user account by his or her user_id. Private accounts may be viewed by logged in users only.
- **GET** /users/me - look current user's account.
- **PUT** /users/me - edit current user's account details.
- **DELETE** /users/me - delete current user's account.
- **POST** /token/refresh - create new access token for current user.
- **GET** /posts - view all posts. Private posts may be viewed by logged in users only.
- **POST** /posts/create - create a post.
- **GET** /posts/post_id - view post by post_id. Private posts may be viewed by logged in users only.
- **PUT** /posts/post_id - edit post by post_id. Only post owner can perform this operation.
- **DELETE** /posts/post_id - delete post by post_id. Only post owner can perform this operation.
- **POST** /posts/post_id/like - like post by post_id. Every logged in user but for post owner can perform this operation.
- **DELETE** /posts/post_id/like = unlike post by post_id. Every logged in user but for post owner can perform this operation.

## Testing

Endpoints tests reside within *tests* directory. They may be launched from the parent directory by the following command:
```
$ python -m unittest discover
```
Normally, testing should return `OK` message.