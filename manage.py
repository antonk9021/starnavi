# -*- coding: utf-8 -*-
"""
This module manages database creation and sets up its migrations.

This script should be called every time the database models change
with `db migrate` and `db upgrade` command line arguments.

Arguments:
    db init -- initializes migration tool (Alembic) and creates migrations repository
    db migrate -- Performs migration; creates SQLite database file if necessary
    db upgrade -- Applies performed migration to the database.

Example:
    $ python manage.py db init
    $ python manage.py db migrate
    $ python manage.py db upgrade
"""

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app.app import create_app, db

app = create_app()
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command("db", MigrateCommand)

if __name__ == "__main__":
    manager.run()
