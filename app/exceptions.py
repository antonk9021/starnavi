# -*- coding: utf-8 -*-
"""
This module sets up custom exceptions needed for narrow scopes of use.
"""


class EmptyRequest(Exception):
    """
    Is raised on signup on empty request.
    """
    def __init__(self):
        self.message = "Empty request"


class PrivacyException(Exception):
    """
    Is raised on unauthorized attempt to view private account.
    """
    def __init__(self):
        self.message = "Login required"


class ExternalApiException(Exception):
    """
    Is raised on hunter.io and Clearbit Enrichment API failures.
    """
    def __init__(self, api):
        self.message = f"Warning: {api} failed. Please check your API key or contact support."
