# -*- coding: utf-8 -*-
"""
This module sets up SQLAlchemy models for interacting with the database
as well as respective schemata for (de)serialization of the data
which is supposed to be written into the database or extracted from it.
"""

from flask_sqlalchemy import SQLAlchemy
from marshmallow import fields, Schema
from datetime import datetime
from app.exceptions import PrivacyException
from passlib.hash import pbkdf2_sha256 as sha256
# from abc import ABCMeta, abstractmethod  # May be used further for an abstract model class


db = SQLAlchemy()


class CustomModelMixin(object):
    """
    Generic interface common among models.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls, exclude_private=False):
        if exclude_private:
            return db.session.query(cls).filter_by(private=False).all()
        return db.session.query(cls).all()

    @classmethod
    def get_single(cls, id_, exclude_private=False):
        """
        Retrieve a single record from the DB.
        :param id_: Record ID to be looked for.
        :param exclude_private: Exclude records not intended for unauthorized viewers.
        """
        result = db.session.query(cls).get(id_)
        if result is not None:
            if exclude_private and result.private:
                raise PrivacyException
        return result


class UserModel(CustomModelMixin, db.Model):
    """
    Users.
    """
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    emailhunter = db.Column(db.String(11))
    password = db.Column(db.String(32), nullable=False)
    reg_date = db.Column(db.DateTime)
    upd_date = db.Column(db.DateTime)
    last_login = db.Column(db.DateTime)
    private = db.Column(db.Boolean)
    posts = db.relationship("PostModel", backref="users", lazy="select", cascade="all, delete-orphan")  # One to many
    likes = db.relationship("PostModel", secondary="likes")  # Many to many
    clearbit_personal = db.relationship("ClearbitPersonalModel",
                                        backref="user",
                                        cascade="all, delete-orphan",
                                        uselist=False)  # One to one
    clearbit_social = db.relationship("ClearbitSocialModel",
                                        backref="user",
                                        cascade="all, delete-orphan",
                                        uselist=False)  # One to one
    clearbit_employment = db.relationship("ClearbitEmploymentModel",
                                          backref="user",
                                          cascade="all, delete-orphan",
                                          uselist=False)  # One to one

    def update(self, data):
        for key, value in data.items():
            if key == "password":
                self.password = self.generate_passwd_hash(key)
            else:
                self[key] = value
        self.upd_date = datetime.utcnow()
        db.session.commit()

    @staticmethod
    def generate_passwd_hash(passwd):
        return sha256.hash(passwd)

    @staticmethod
    def verify_passwd_hash(raw_passwd, passwd_hash):
        return sha256.verify(raw_passwd, passwd_hash)

    @classmethod
    def find_by_email(cls, email):
        """
        Checks whether user`s email already exists within the database.
        :param email: email to be looked for.
        """
        return cls.query.filter_by(email=email).first()


class PostModel(CustomModelMixin, db.Model):
    """
    Posts.
    """
    __tablename__ = "posts"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256), nullable=False)
    content = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    owner_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    private = db.Column(db.Boolean)
    likes = db.relationship("UserModel", secondary="likes")  # Many to many

    def update(self, data):
        for key, value in data.items():
            self[key] = value
        self.modified_at = datetime.utcnow()
        db.session.commit()


class LikeModel(CustomModelMixin, db.Model):
    """
    Posts likes by users.
    """
    __tablename__ = "likes"
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey("posts.id"), nullable=False)
    liked_by = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)

    @classmethod
    def search(cls, post_id, liked_by):
        return cls.query.filter_by(post_id=post_id, liked_by=liked_by).first()


class RevokedTokenModel(CustomModelMixin, db.Model):
    """
    Revoked access tokens.
    """
    __tablename__ = "revoked_tokens"
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    @classmethod
    def is_jti_blacklisted(cls, jti):
        """
        Checks whether respective JWT ID is within the blacklist
        :param jti: JWT ID to be checked
        """
        return bool(cls.query.filter_by(jti=jti).first())


class ClearbitPersonalModel(CustomModelMixin, db.Model):
    """
    Personal info from Clearbit.
    """
    __tablename__ = "clearbit_personal"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    given_name = db.Column(db.String(32))
    family_name = db.Column(db.String(32))
    city = db.Column(db.String(64))
    state = db.Column(db.String(64))
    country = db.Column(db.String(2))
    bio = db.Column(db.String(256))
    site = db.Column(db.String(253))  # Maximum length of full domain name including dots


class ClearbitSocialModel(CustomModelMixin, db.Model):
    """
    Social media info from Clearbit
    """
    __tablename__ = "clearbit_social"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    facebook_handle = db.Column(db.String(50))  # Maximum length of Facebook handle
    linkedin_handle = db.Column(db.String(50))
    github_handle = db.Column(db.String(39))  # Maximum length of GitHub handle
    github_id = db.Column(db.Integer)
    github_followers = db.Column(db.Integer)
    twitter_handle = db.Column(db.String(15))  # Maximum length of Twitter handle
    twitter_followers = db.Column(db.Integer)


class ClearbitEmploymentModel(CustomModelMixin, db.Model):
    """
    Employment info from Clearbit.
    """
    __tablename__ = "clearbit_employment"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey("clearbit_companies.id"))
    title = db.Column(db.String(64))
    role = db.Column(db.String(64))
    subrole = db.Column(db.String(64))
    seniority = db.Column(db.String(32))
    company = db.relationship("ClearbitCompanyModel", backref="employees")  # Many to one


class ClearbitCompanyModel(CustomModelMixin, db.Model):
    """
    General company info from Clearbit.
    """
    __tablename__ = "clearbit_companies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    legal_name = db.Column(db.String(256), unique=True)
    sector = db.Column(db.String(32))
    industry = db.Column(db.String(64))
    sic_code = db.Column(db.Integer)
    naics_code = db.Column(db.Integer)
    hq_city = db.Column(db.String(64))
    hq_state = db.Column(db.String(32))
    hq_country = db.Column(db.String(2))
    phone = db.Column(db.String(20))
    domain = db.Column(db.String(253))
    metrics = db.relationship("ClearbitMetricsModel",
                              backref="company",
                              cascade="all, delete-orphan",
                              uselist=False)  # One to one

    @classmethod
    def find_by_legal_name(cls, legal_name):
        """
        Retrieve a single company by its legal name from the DB.
        :param legal_name: A legal name to be looked for within the database.
        """
        return cls.query.filter_by(legal_name=legal_name).first()


class ClearbitMetricsModel(CustomModelMixin, db.Model):
    """
    Company metrics from Clearbit.
    """
    __tablename__ = "clearbit_metrics"
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey("clearbit_companies.id"), nullable=False)
    raised = db.Column(db.Integer)
    alexa_us = db.Column(db.Integer)
    alexa_global = db.Column(db.Integer)
    annual_revenue = db.Column(db.Integer)


# SCHEMATA


class PostSchema(Schema):
    """
    Marshmallow schema for (de)serializing posts-related data
    """
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True)
    content = fields.Str(required=True)
    owner_id = fields.Int(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)
    private = fields.Boolean(load_only=True)


class UserSchema(Schema):
    """
    Marshmallow schema for (de)serializing users-related data
    """
    id = fields.Int(dump_only=True)
    name = fields.String(required=True)
    email = fields.Email(required=True)
    emailhunter = fields.String(load_only=True)
    last_login = fields.DateTime(dump_only=True)
    password = fields.Str(required=True, load_only=True)
    reg_date = fields.DateTime(dump_only=True)
    private = fields.Boolean(load_only=True)
    posts = fields.Nested(PostSchema, many=True)




class ClearbitCompanySchema(Schema):
    """
    Marshmallow schema for (de)serializing company-related data from Clearbit
    """
    id = fields.Int(dump_only=True)
    name = fields.String()
    legal_name = fields.String()
    sector = fields.String()
    industry = fields.String()
    sic = fields.String()
    naics_code = fields.String()
    hq_city = fields.String()
    hq_state = fields.String()
    hq_country = fields.String()
    phone = fields.String()
    site = fields.String()
