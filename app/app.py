# -*- coding: utf-8 -*-
"""
This module creates a database and a Flask application instance, sets up its configuration,
JWT capabilities, and API endpoints, as well as initializes the database.
Fully set up application instance is intended to be called from run.py.
"""

from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from config import DevConfig, TestingConfig
from app.models import db, RevokedTokenModel
from app import resources as res


jwt = JWTManager()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """
    Checks whether JWT token is in the blacklist before accessing protected endpoint.
    :param decrypted_token: JWT token to be checked.
    """
    jti = decrypted_token["jti"]
    return RevokedTokenModel.is_jti_blacklisted(jti)


def create_app(config_name="dev"):
    """
    Initializes Flask app instance.
    :return:
    """
    app = Flask(__name__)
    if config_name.lower() == "dev":
        app.config.from_object(DevConfig)
    if config_name.lower() == "testing":
        app.config.from_object(TestingConfig)
    db.init_app(app)
    api = Api(app=app)
    jwt.init_app(app)

    api.add_resource(res.AllUsers, '/', "/users")
    api.add_resource(res.UserSignUp, "/users/signup")
    api.add_resource(res.UserLogin, "/users/login")
    api.add_resource(res.UserLogoutAccess, "/users/logout", "/users/logout/access")
    api.add_resource(res.UserLogoutRefresh, "/users/logout/refresh")
    api.add_resource(res.UserById, "/users/<int:user_id>")
    api.add_resource(res.MyPage, "/users/me")
    api.add_resource(res.TokenRefresh, "/token/refresh")
    api.add_resource(res.AllPosts, "/posts")
    api.add_resource(res.PostCreation, "/posts/create")
    api.add_resource(res.PostById, "/posts/<int:post_id>")
    api.add_resource(res.PostLike, "/posts/<int:post_id>/like")
    api.add_resource(res.PostsByUser, "/users/<int:user_id>/posts")

    return app
