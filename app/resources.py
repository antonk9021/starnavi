# -*- coding: utf-8 -*-
"""
This module sets up API resources.
"""

from flask import request, Response, json
from flask_restful import Resource
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_optional, get_jwt_identity,
                                jwt_refresh_token_required, get_raw_jwt, jwt_required)
from app.models import *
from marshmallow.exceptions import ValidationError
from app.exceptions import *
import requests
from requests.exceptions import HTTPError
import os
from functools import wraps
import clearbit
import logging

user_schema = UserSchema()
post_schema = PostSchema()
company_schema = ClearbitCompanySchema()

# Server-side app-level exceptions and warnings:
logging.basicConfig(filename="app.log", level=logging.WARNING)
logger = logging.getLogger("app_logger")


def jwt_prohibited(fn):
    """
    Prevent the situation when logged in user tries to register a new account.
    Inspired by original `jwt_required` decorator from flask-jwt-extended:

    https://flask-jwt-extended.readthedocs.io/en/stable/_modules/flask_jwt_extended/view_decorators/#jwt_required

    :param fn: method to be decorated.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if "Authorization" in request.headers:
            return Response(
                mimetype="application/json",
                response=json.dumps({"error": "Please log out prior to registering a new user"}),
                status=403
            )
        return fn(*args, **kwargs)
    return wrapper


class CustomResource(Resource):
    """
    Generic interface commonly used among resources.
    """
    @staticmethod
    def response(body, status_code):
        """
        Creates response with custom body.
        :param body: response body.
        :param status_code: response status code.
        """
        return Response(
            mimetype="application/json",
            response=json.dumps(body),
            status=status_code
        )

    @staticmethod
    def _validate_user(req, partial=None):
        """
        Check request for valid user data.
        :param req: request to be checked.
        """
        if not bool(req.data):
            raise EmptyRequest

        request_data = req.get_json()

        # Missing email or password will result in ValidationError:
        try:
            return user_schema.load(request_data, partial=partial)
        except ValidationError:
            raise

    @staticmethod
    def _validate_post(req, jwt_identity):
        """
        Check request for valid post data.
        :param req: request to be checked.
        """
        if not bool(req.data):
            raise EmptyRequest

        request_data = req.get_json()
        request_data["owner_id"] = jwt_identity
        data = post_schema.load(request_data)

        return data


class UserSignUp(CustomResource):
    @jwt_prohibited
    def post(self):
        """
        New user registration.
        """
        try:
            data = self._validate_user(request)
        except (EmptyRequest, ValidationError) as e:
            return self.response({"error": e.message if hasattr(e, "message") else e.messages}, 400)

        if UserModel.find_by_email(data.get("email")):
            error = {"error": f"User with email {data.get('email')} already exists"}
            return self.response(error, 400)

        try:
            email_status = self._verify_with_emailhunter(data.get("email"))
        except ExternalApiException as e:
            logger.warning(e.message)
            email_status = None

        try:
            clearbit_info = self._get_clearbit_info(data.get("email"))
        except (ExternalApiException, HTTPError) as e:
            logger.warning(e.message if hasattr(e, "message") else e)
            clearbit_info = None

        hashed_password = UserModel.generate_passwd_hash(data.get("password"))

        user = UserModel(
            name=data.get("name"),
            email=data.get("email"),
            emailhunter=email_status,
            password=hashed_password,
            reg_date=datetime.utcnow(),
            private=bool(data.get("private"))
        )

        user.save()

        if clearbit_info is not None:
            self._write_clearbit_data(clearbit_info, user)

        return self.response({"msg": f"User {data.get('name')} has been created"}, 201)

    @staticmethod
    def _verify_with_emailhunter(email):
        """
        Verify an email through hunter.io.
        :param email: An email to be verified.
        """
        ehunter_key = os.environ.get("EMAILHUNTER_KEY")
        if not ehunter_key:
            raise ExternalApiException("emailhunter")
        ver_url = f"https://api.hunter.io/v2/email-verifier?email={email}&api_key={ehunter_key}"
        try:
            result = requests.get(ver_url)
        except HTTPError:
            return "No result"
        if result.status_code < 400:
            return result.json()["data"]["result"].lower()
        return "No result"

    @staticmethod
    def _get_clearbit_info(email):
        """
        Try to get data from clearbit.com by user`s email.
        :param email: User`s email which the data will be gathered by.
        """
        clearbit.key = os.environ.get("CLEARBIT_KEY")
        if not clearbit.key:
            raise ExternalApiException(api="clearbit")
        try:
            return clearbit.Enrichment.find(email=email, stream=True)
        except HTTPError:
            raise

    def _write_clearbit_data(self, data, user):
        """
        Writes data from Clearbit into the database. 
        :param data: Info retrieved from Clearbit.
        """
        person = data["person"]
        company = data["company"]
        user_data = user_schema.dump(user)

        if person is not None:
            given_name, family_name = self._unpack(person["name"], "givenName", "familyName")
            city, state, country = self._unpack(person["geo"], "city", "state", "countryCode")

            clearbit_personal = ClearbitPersonalModel(
                user_id=user_data["id"],
                given_name=given_name,
                family_name=family_name,
                city=city,
                state=state,
                country=country,
                bio=person["bio"],
                site=person["site"]
            )
            clearbit_personal.save()

            facebook_handle, linkedin_handle, github_handle, twitter_handle = \
                (person[key]["handle"] for key in ["facebook", "linkedin", "github", "twitter"])

            github_followers, twitter_followers = (person[key]["followers"] for key in ["github", "twitter"])

            clearbit_social = ClearbitSocialModel(
                user_id=user_data["id"],
                facebook_handle=facebook_handle,
                linkedin_handle=linkedin_handle,
                github_handle=github_handle,
                github_id=person["github"]["id"],
                github_followers=github_followers,
                twitter_handle=twitter_handle,
                twitter_followers=twitter_followers
            )
            clearbit_social.save()

        if company is not None:
            company_found = ClearbitCompanyModel.find_by_legal_name(company["legalName"])
            company_data = company_schema.dump(company_found)
            if not company_data:
                sector, industry, sic_code, naics_code = self._unpack(company["category"],
                                                                      "sector", "industry", "sic_code", "naics_code")
                hq_city, hq_state, hq_country = self._unpack(company["geo"], "city", "state", "countryCode")

                clearbit_company = ClearbitCompanyModel(
                    name=company["name"],
                    legal_name=company["legalName"],
                    sector=sector,
                    industry=industry,
                    sic_code=sic_code,
                    naics_code=naics_code,
                    hq_city=hq_city,
                    hq_state=hq_state,
                    hq_country=hq_country,
                    phone=company["phone"],
                    domain=company["domain"]
                )
                clearbit_company.save()
                company_data = company_schema.dump(clearbit_company)

                raised, alexa_us, alexa_global, annual_revenue = self._unpack(company["metrics"],
                                                                              "raised", "alexaUsRank",
                                                                              "alexaGlobalRank",
                                                                              "annualRevenue")

                clearbit_metrics = ClearbitMetricsModel(
                    company_id=company_data["id"],
                    raised=raised,
                    alexa_us=alexa_us,
                    alexa_global=alexa_global,
                    annual_revenue=annual_revenue
                )
                clearbit_metrics.save()
            if person is not None:
                title, role, subrole, seniority = self._unpack(person["employment"],
                                                               "title", "role", "subrole", "seniority")

                clearbit_employment = ClearbitEmploymentModel(
                    user_id=user_data["id"],
                    company_id=company_data["id"],
                    title=title,
                    role=role,
                    subrole=subrole,
                    seniority=seniority
                )
                clearbit_employment.save()

    @staticmethod
    def _unpack(dict_like, *keys):
        """
        An auxiliary data for `unpacking` nested dict-like data.
        :param dict_like: A dict-like object to be unpacked.
        :param keys: Keys indicating relevant values to be unpacked.
        """
        return map(dict_like.get, keys)


class UserLogin(CustomResource):
    def post(self):
        """
        Log in an existing user.
        """
        try:
            data = self._validate_user(request, partial=("name",))
        except (EmptyRequest, ValidationError) as e:
            return self.response({"error": e.message if hasattr(e, "message") else e.messages}, 400)

        user = UserModel.find_by_email(data.get("email"))

        if not user:
            return self.response({"error": f"User with email {data.get('email')} doesn\'t exist"}, 404)

        try:
            UserModel.verify_passwd_hash(data.get("password"), user.password)
        except ValueError:
            return self.response({"error": "Invalid credentials"}, 400)

        user.last_login = datetime.utcnow()
        user.save()

        message = "Logged in as {}"
        tokens = self._create_jwt_tokens(user, message)

        return self.response(tokens, 200)

    @staticmethod
    def _create_jwt_tokens(user, message):
        """
        Create access and refresh JWT tokens for a registered user.
        :param user: User for whom the tokens are intended
        :param message: Login message to be returned with tokens
        """
        serialized_data = user_schema.dump(user)
        access_token = create_access_token(identity=serialized_data["id"])
        refresh_token = create_refresh_token(identity=serialized_data["id"])

        return {"msg": message.format(serialized_data["name"]),
                "access_token": access_token,
                "refresh_token": refresh_token}


class AllUsers(CustomResource):
    @jwt_optional
    def get(self):
        """
        Retrieves list of accounts. If no current user is logged in,
        returns only accounts that are not private.
        """
        exclude_private = (get_jwt_identity() is None)
        users = UserModel.get_all(exclude_private)
        serialized_users = user_schema.dump(users, many=True)

        if not len(serialized_users):
            return self.response({"error": "No public user accounts found"}, 404)

        return self.response(serialized_users, 200)


class UserLogoutAccess(CustomResource):
    @jwt_required
    def post(self):
        """
        User logout with access token revoking.
        """
        jti = get_raw_jwt()["jti"]
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.save()

        return self.response({"msg": "Access token has been revoked"}, 200)


class UserLogoutRefresh(CustomResource):
    @jwt_refresh_token_required
    def post(self):
        """
        User logout with refresh token revoking.
        """
        jti = get_raw_jwt()["jti"]
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.save()

        return self.response({"msg": "Refresh token has been revoked"}, 200)


class UserById(CustomResource):
    @jwt_optional
    def get(self, user_id):
        """
        Get info on user.
        :param user_id: ID of the user whose info will be retrieved.
        """
        exclude_private = (get_jwt_identity() is None)
        try:
            user_to_view = UserModel.get_single(user_id, exclude_private)
        except PrivacyException as e:
            return self.response({"error": e.message}, 401)
        data_to_view = user_schema.dump(user_to_view)
        if not len(data_to_view):
            return self.response({"error": "Account not found"}, 404)

        return self.response(data_to_view, 200)


class MyPage(CustomResource):
    @jwt_required
    def get(self):
        """
        Get my account info.
        """
        user = UserModel.get_single(get_jwt_identity())
        serialized_user = user_schema.dump(user)
        if not len(serialized_user):
            return self.response({"error": "Your account was deleted"}, 404)
        return self.response(serialized_user, 200)

    @jwt_required
    def put(self):
        """
        Update my account details.
        """
        try:
            data = self._validate_user(request, partial=True)
        except (EmptyRequest, ValidationError) as e:
            return self.response({"error": e.message if hasattr(e, "message") else e.messages}, 400)

        user = UserModel.get_single(get_jwt_identity())
        serialized_user = user_schema.dump(user)
        if not len(serialized_user):
            return self.response({"error": "Your account was deleted"}, 404)

        # If I want my account to become private/public,
        # all my posts will respectively become private/public:
        if "private" in data.keys():
            for post in user.posts:
                post.update({"private": data["private"]})
        user.update(data)
        serialized_user = user_schema.dump(user)

        return self.response(serialized_user, 200)

    @jwt_required
    def delete(self):
        """
        Delete my account.
        WARNING: Database record for the user will be removed.
        """
        user = UserModel.get_single(get_jwt_identity())
        if not user:
            return self.response({"error": "Your account was deleted"}, 404)
        user.delete()

        return self.response({"msg": "Deleted"}, 204)


class AllPosts(CustomResource):
    @jwt_optional
    def get(self):
        """
        Retrieve all posts.
        """
        exclude_private = (get_jwt_identity() is None)
        posts = PostModel.get_all(exclude_private)
        data = post_schema.dump(posts, many=True)
        if not len(data):
            return self.response({"error": "No public posts have been found"}, 404)
        return self.response(data, 200)


class PostCreation(CustomResource):
    @jwt_required
    def post(self):
        """
        Create a post.
        """
        try:
            data = self._validate_post(request, get_jwt_identity())
        except (EmptyRequest, ValidationError) as e:
            return self.response({"error": e.message if hasattr(e, "message") else e.messages}, 400)

        user = UserModel.get_single(get_jwt_identity())

        # User with deleted account should not create any posts
        # even despite valid access token:
        if not user:
            return self.response({"error": "You cannot create a post since your account was deleted"}, 403)

        post = PostModel(
            title=data.get("title"),
            content=data.get("content"),
            owner_id=data.get("owner_id"),
            created_at=datetime.utcnow(),
            private=user.private
        )

        post.save()
        serialized_post = post_schema.dump(post)

        return self.response(serialized_post, 201)


class PostById(CustomResource):
    @jwt_optional
    def get(self, post_id):
        """
        Retrieve a post by its ID.
        :param post_id: ID of the post to be retrieved.
        """
        exclude_private = (get_jwt_identity() is None)
        try:
            post_to_view = PostModel.get_single(post_id, exclude_private)
        except PrivacyException as e:
            return self.response({"error": e.message}, 401)

        data_to_view = post_schema.dump(post_to_view)

        return self.response(data_to_view, 200)

    @jwt_required
    def put(self, post_id):
        """
        Edit post.
        :param post_id: ID of the post which will be edited.
        """
        try:
            data = self._validate_post(request, get_jwt_identity())
        except (EmptyRequest, ValidationError) as e:
            return self.response({"error": e.message if hasattr(e, "message") else e.messages}, 400)
        post = PostModel.get_single(post_id)
        if post.owner_id != get_jwt_identity():
            return self.response({"error": "Only post owner can edit it"}, 403)

        post.update(data)
        serialized_post = post_schema.dump(post)

        return self.response(serialized_post, 200)

    @jwt_required
    def delete(self, post_id):
        """
        Delete a post by its ID.
        :param post_id: ID of the post which will be deleted.
        """
        post = PostModel.get_single(post_id)

        if post.owner_id != get_jwt_identity():
            return self.response({"error": "Only post owner can delete it"}, 403)

        post.delete()

        return self.response({"msg": "Deleted"}, 204)


class PostsByUser(CustomResource):
    @jwt_optional
    def get(self, user_id):
        """
        Retrieve all posts created by one user.
        """
        exclude_private = (get_jwt_identity() is None)
        try:
            user_to_view = UserModel.get_single(user_id, exclude_private)
        except PrivacyException as e:
            return self.response({"error": e.message}, 401)
        user_data = user_schema.dump(user_to_view)
        if not len(user_data):
            return self.response({"error": "Account not found"}, 404)
        posts = user_data["posts"]

        return self.response({"msg": posts}, 200)


class PostLike(CustomResource):
    """
    Like or unlike a post.
    """
    @jwt_required
    def post(self, post_id):
        """
        Like.
        """
        post = PostModel.get_single(post_id)
        if not post:
            return self.response({"error": f"Post #{post_id} not found."}, 404)

        serialized_post = post_schema.dump(post)

        if serialized_post["owner_id"] == get_jwt_identity():
            return self.response({"error": "Sorry, you cannot like your own post"}, 403)

        if LikeModel.search(post_id, serialized_post["owner_id"]):
            return self.response({"error": "Sorry, you cannot like the same post twice"}, 403)

        likes = LikeModel(post_id=serialized_post["id"], liked_by=serialized_post["owner_id"])
        likes.save()

        return self.response({"msg": f"You have liked the post #{serialized_post.get('id')}"}, 200)

    @jwt_required
    def delete(self, post_id):
        """
        Unlike.
        """
        post = PostModel.get_single(post_id)
        if not post:
            return self.response({"error": f"Post #{post_id} not found."}, 404)

        serialized_post = post_schema.dump(post)

        if serialized_post["owner_id"] == get_jwt_identity():
            return self.response({"error": "Sorry, you cannot unlike your own post"}, 403)

        like = LikeModel.search(post_id, serialized_post["owner_id"])

        if not like:
            return self.response({"error": "You cannot unlike a post you haven\'t liked in the past"}, 403)

        like.delete()

        return self.response({"msg": f"You have unliked the post #{serialized_post.get('id')}"}, 200)


class TokenRefresh(CustomResource):
    @jwt_refresh_token_required
    def post(self):
        """
        Refresh access token for current user.
        """
        access_token = create_access_token(identity=get_jwt_identity())

        return {"new access token": access_token}, 200
